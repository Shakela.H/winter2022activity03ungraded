public class Application{
	public static void main(String [] args){
		Parrot firstParrot = new Parrot();
		firstParrot.gender = "Male";
		System.out.println(firstParrot.gender);
		firstParrot.age = 5;
		System.out.println(firstParrot.age);
		firstParrot.color = "Blue";
		System.out.println(firstParrot.color);
		
		
		firstParrot.dance();//
		
		Parrot secondParrot = new Parrot();
		secondParrot.gender = "Female";
		System.out.println(secondParrot.gender);
		secondParrot.age = 8;
		System.out.println(secondParrot.age);
		secondParrot.color = "Green";
		System.out.println(secondParrot.color);
		
		secondParrot.speak();
		
		Parrot[] flock = new Parrot[3]; //fun fact, group of parrots also knowns as a pandemonium 
		flock[0] = firstParrot;
		flock[1] = secondParrot;
		flock[2] = new Parrot();
		flock[2].gender = "Female";
		flock[2].age = 2;
		flock[2].color = "Yellow";
		System.out.println("3rd bird gender: "+ flock[2].gender +", "+"Age: "+flock[2].age +", " +"Color: "+flock[2].color);
	}
}
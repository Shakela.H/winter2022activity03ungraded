public class Parrot{
	public String gender;
	public int age;
	public String color;
	
	public void speak(){
		if (this.color.equals("Green")){
			System.out.println("Hi! I am a green parrot");
		}else if (this.color.equals("Blue")){
			System.out.println("Hi! I am a Blue parrot");
		}else{
			System.out.println("Hi! I don't know what color I am. I am color blind");
		}
	}
	public void dance(){
		if (this.gender.equals("Male")){
			System.out.println("Look at me, I'm a male parrot. I dance to impress!");
		}else{
			System.out.println("male parrots likes to dance to impress. I like to watch.");
		}
	}
}